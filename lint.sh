#!/bin/sh
set -xe
flake8 ewd
pylint --rcfile setup.cfg ewd
