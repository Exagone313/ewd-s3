# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) 2023 Elouan Martinet <exa@elou.world>

from os import getenv
from secrets import token_hex
from asyncio import sleep
import logging
from mimetypes import guess_type
import json
import re


import botocore.session
from botocore.exceptions import (
    ClientError,
    NoCredentialsError,
)
from quart import Quart, Response, request, abort
import nacl.pwhash
import nacl.exceptions


app = Quart("ewd")
logger = logging.getLogger("ewd")


max_content_length = getenv("MAX_CONTENT_LENGTH")
if max_content_length:
    try:
        max_content_length = int(max_content_length)
        app.config["MAX_CONTENT_LENGTH"] = max_content_length
    except ValueError as _exc:
        logger.exception(_exc)
        app.config["MAX_CONTENT_LENGTH"] = 0


def get_config():
    auth = {}
    auth_data = getenv("AUTH_DATA")
    if not auth_data:
        auth_file = getenv("AUTH_FILE")
        if auth_file:
            try:
                with open(auth_file, encoding="utf-8") as fobj:
                    auth_data = fobj.read()
            except (OSError, ValueError) as exc:
                logger.exception(exc)
    if auth_data:
        try:
            auth = json.loads(auth_data)
            assert isinstance(auth, dict)
            for username, password in auth.items():
                assert isinstance(username, str)
                assert isinstance(password, str)
        except (json.JSONDecodeError, AssertionError) as exc:
            logger.exception(exc)
            auth = {}
    return {
        "auth": auth,
        "disable_auth": getenv("DISABLE_AUTH", "").lower()[:1] in ("y", "1", "t"),
        "require_auth": getenv("REQUIRE_AUTH", "").lower()[:1] in ("y", "1", "t"),
        "url": getenv("URL_PREFIX", "http://127.0.0.1:8000"),
        "bucket": getenv("S3_BUCKET", "ewd"),
        "prefix": getenv("S3_PREFIX", ""),
        "prefix_fallback": getenv("S3_PREFIX_FALLBACK", ""),
        "endpoint_url": getenv("S3_ENDPOINT_URL", "https://s3.amazonaws.com"),
        "region": getenv("S3_REGION", "us-east-1"),
        "enable_fallback": getenv("ENABLE_FALLBACK", "").lower()[:1] in ("y", "1", "t"),
    }


def authenticate(config):
    if config["disable_auth"] is True:
        return
    authorization = request.headers.get("Authorization")
    if authorization and ":" in authorization:
        username, password = authorization.split(":", 1)
        if username not in config["auth"]:
            nacl.pwhash.argon2id.str(b"fake-ewd-password-string")
        else:
            try:
                nacl.pwhash.verify(config["auth"][username].encode("utf-8"), password.encode("utf-8"))
                return
            except (nacl.exceptions.InvalidkeyError, nacl.exceptions.CryptPrefixError) as exc:
                if isinstance(exc, nacl.exceptions.CryptPrefixError):
                    logger.exception(exc)
    abort(403)
    raise RuntimeError("fell through")


def generate_slug():
    return token_hex(3)


SLUG_RE = re.compile(r"^[a-z0-9]{6}$")


def matches_slug(slug):
    return SLUG_RE.fullmatch(slug) is not None


async def read_sent_file():
    files = await request.files
    file_list = list(files.keys())
    if len(file_list) > 0:
        sent_file = files.get(file_list[0])
        if sent_file:
            filename = sent_file.filename
            extension = None
            if "." in filename:
                extension = filename.split(".", 1)[1].lower() or None
            return sent_file.read(), extension
    return None, None


def find_mimetype(filename):
    default = "text/plain"
    if "." not in filename:
        return default
    extension = filename.split(".", 1)[1].lower()
    if not extension:
        return default
    guessed, _ = guess_type(filename, strict=False)
    if guessed is None:
        return "application/octet-stream"
    return guessed


def get_s3_client(endpoint_url, region_name):
    session = botocore.session.get_session()
    return session.create_client("s3", endpoint_url=endpoint_url, region_name=region_name)


def download_file(s3, bucket, key):
    s3_object = s3.get_object(Bucket=bucket, Key=key)
    headers = s3_object["ResponseMetadata"]["HTTPHeaders"]
    return {
        "data": s3_object["Body"].read(),
        "type": headers["content-type"],
    }


def file_exists(s3, bucket, key):
    try:
        s3.head_object(Bucket=bucket, Key=key)
        return True
    except (s3.exceptions.NoSuchKey, ClientError):
        return False


def upload_file(s3, bucket, key, data):
    s3.put_object(Bucket=bucket, Key=key, Body=data)


@app.errorhandler(403)
def forbidden(_e):
    return {"error": "authentication failed"}, 403


@app.errorhandler(404)
def not_found(_e):
    return {"error": "not found"}, 404


@app.errorhandler(413)
def entity_too_large(_e):
    return {"error": "too large"}, 413


@app.errorhandler(500)
def internal_server_error(_e):
    return {"error": "internal"}, 500


@app.route("/")
async def print_welcome():
    config = get_config()
    if config["require_auth"]:
        authenticate(config)
    return {"status": "welcome to ewd"}


@app.route("/<filename>")
async def get_file(filename: str):
    config = get_config()
    if config["require_auth"]:
        authenticate(config)
    try:
        s3 = get_s3_client(config["endpoint_url"], config["region"])
    except (ClientError, NoCredentialsError) as exc:
        logger.exception(exc)
        return {"error": "internal"}, 500
    slug = filename.split(".", 1)[0]
    if matches_slug(slug):
        key = f"{config['prefix']}{slug}"
        try:
            result = download_file(s3, config["bucket"], key)
            mimetype = result["type"]
            if mimetype == "application/octet-stream":
                mimetype = find_mimetype(filename)
            return Response(result["data"], mimetype=mimetype)
        except s3.exceptions.NoSuchKey:
            pass
        except (ClientError, NoCredentialsError) as exc:
            logger.exception(exc)
            return {"error": "internal"}, 500
    if config["enable_fallback"]:
        fallback_key = f"{config['prefix_fallback']}{filename}"
        try:
            result = download_file(s3, config["bucket"], fallback_key)
            mimetype = result["type"]
            if mimetype == "application/octet-stream":
                mimetype = find_mimetype(filename)
            return Response(result["data"], mimetype=mimetype)
        except s3.exceptions.NoSuchKey:
            pass
        except (ClientError, NoCredentialsError) as exc:
            logger.exception(exc)
            return {"error": "internal"}, 500
    return {"error": "not found"}, 404


@app.route("/", methods=["POST"])
async def post_file():
    config = get_config()
    authenticate(config)
    data, extension = await read_sent_file()
    if not data:
        return {"error": "no file sent"}, 400
    try:
        s3 = get_s3_client(config["endpoint_url"], config["region"])
        while True:
            slug = generate_slug()
            key = f"{config['prefix']}{slug}"
            # FIXME there is a potential race condition if multiple files are uploaded in parallel
            if not file_exists(s3, config["bucket"], key):
                break
            await sleep(0.2)
        upload_file(s3, config["bucket"], key, data)
        result_url = f"{config['url']}/{slug}"
        if extension:
            result_url += f".{extension}"
        if "json" in request.headers.get("Accept", "").lower():
            return {"url": result_url}
        return Response(f"{result_url}\n", mimetype="text/plain")
    except (ClientError, NoCredentialsError) as exc:
        logger.exception(exc)
        return {"error": "internal"}, 500
