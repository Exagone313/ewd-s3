FROM docker.io/python:3.11.5-bookworm
RUN mkdir -p /usr/src/ewd
COPY requirements.txt /usr/src/ewd/
RUN pip install -r /usr/src/ewd/requirements.txt
COPY setup.py setup.cfg COPYING /usr/src/ewd/
COPY ewd/ /usr/src/ewd/ewd
RUN cd /usr/src/ewd && pip install -r requirements.txt . && rm -rf /usr/src/ewd
CMD ["hypercorn", "ewd:app", "-b", "0.0.0.0:8000"]
