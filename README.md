# ewd

This is a rewritten version of [ewd](https://gitlab.com/Exagone313/ewd) made in Python.

## Prerequisites

It is tested working with Python 3.11.5.

Files are stored in an S3 bucket so that deployment can be stateless. Local filesystem is not supported.

## Environment variables

To configure ewd, some environment variables can be set:

| Variable | Default value | Description |
| --- | --- | --- |
| `URL_PREFIX` | `http://127.0.0.1:8000` | Value prefixed to URL returned when uploading a file |
| `S3_BUCKET` | `ewd` | S3 bucket where files are stored |
| `S3_PREFIX` | *empty* | Value prefixed to S3 keys when downloading and uploading a file |
| `S3_PREFIX_FALLBACK` | *empty* | Value prefixed to S3 keys when downloading a file as fallback (requires setting `ENABLE_FALLBACK` to `yes`) |
| `S3_ENDPOINT_URL` | `https://s3.amazonaws.com` | Endpoint used to connect to S3 services |
| `S3_REGION` | `us-east-1` | S3 region name |
| `AWS_ACCESS_KEY_ID` | *empty* | S3 access key |
| `AWS_SECRET_ACCESS_KEY` | *empty* | S3 secret key |
| `AUTH_DATA` | *empty* | Key-value pair of usernames and passwords, hashed by [libsodium](https://libsodium.org/) (e.g using Argon2id algorithm), encoded in JSON, used for authentication (see below for an example) |
| `AUTH_FILE` | *empty* | Path to a file containing authentication data |
| `DISABLE_AUTH` | `no` | Set to `yes` to disable authentication (authentication is enabled by default, even if no credentials are set) |
| `REQUIRE_AUTH` | `no` | Set to `yes` to require authentication on every route, not only when uploading a file (requires `DISABLE_AUTH` to be set to `no`) |
| `MAX_CONTENT_LENGTH` | *empty* | Maximum memory used for file upload processing, needs to be sensibly greater than the expected maximum file size |
| `ENABLE_FALLBACK` | `no` | Set to `yes` to download files with the specified file name as a fallback (see also `S3_PREFIX_FALLBACK`) |

## Run ewd in development

* Create a virtual environment:
	```bash
	python3 -m venv .venv
	```
* Activate it:
	```bash
	. .venv/bin/activate
	```
* Install dependencies and ewd package:
	```bash
	.venv/bin/pip install -r requirements.txt -r requirements.dev.txt -e '.[dev]'
	```
* Run ewd:
	```bash
	.venv/bin/hypercorn ewd:app
	```

By default, ewd will be accessible at <http://127.0.0.1:8000>.

## Run ewd in production

* Create a virtual environment:
	```bash
	python3 -m venv .venv
	```
* Activate it:
	```bash
	. .venv/bin/activate
	```
* Install dependencies and ewd package:
	```bash
	.venv/bin/pip install -r requirements.txt -e .
	```
* Run ewd:
	```bash
	.venv/bin/hypercorn ewd:app -b 0.0.0.0:8000
	```

With `-b 0.0.0.0:8000`, ewd will be accessible on all addresses on port 8000.

For more information on command line arguments, check [hypercorn](https://hypercorn.readthedocs.io/en/latest/how_to_guides/configuring.html) documentation.

## Run in a container

ewd is also available as a container image in the following registries:
* GitLab Registry: `registry.gitlab.com/exagone313/ewd-s3`
* GitHub Registry: `ghcr.io/exagone313/ewd-s3`
* Elouworld Registry: `registry.ewd.app/exagone313/ewd-s3`

It is tested working with [Podman](https://podman.io/), but it should work with other tools.

## API routes

* `GET /`: prints a welcome message, also used for checking authentication when `REQUIRE_AUTH` is set to `yes`
	* returns `{"status":"welcome to ewd"}`
* `POST /`: uploads a file
	* the file needs to be sent using `multipart/form-data`, e.g. by using `curl -v -F f=@path/to/file -H 'Authorization: username:password' http://127.0.0.1:8000/`
	* it only processes the first file found, it doesn't support uploading multiple files
	* returns a link to the uploaded file on success
	* if the header `Accept: application/json` is specified, returns `{"url":"(link to uploaded file)"}` instead
	* returns `{"error":"no file sent"}` if no file was sent
* `GET /{filename}`: downloads a file
	* returns the file on success, with proper mime type in `Content-Type` header
	* if not found, returns `{"error":"not found"}`
* all routes may return `{"error":"internal"}` on failure, an exception stack trace will be printed in console output
* on authentication error, it returns `{"error":"authentication failed"}`

## Example of authentication data

To create authentication data, you can use PyNaCl which is installed with ewd, to hash a password.

Example script:
```python
import json
import nacl.pwhash

print(json.dumps({
    "alice": nacl.pwhash.argon2id.str(b"alice-password-bytes").decode("utf-8"),
	"bob": nacl.pwhash.argon2id.str(b"bob-password-bytes").decode("utf-8"),
}))
```

See `AUTH_DATA` and `AUTH_FILE` environment variables for more information.

## License

```
Copyright (c) 2023 Elouan Martinet <exa@elou.world>

ewd is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
